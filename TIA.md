# TIA

## event consistent with TIA?

* definition:
    * transient focal reduction in blood flow (ischemia)
    * not sufficient to cause death of tissue (which would be a stroke)
    * applied to CNS tissue (brain, spine, retina)
    * old obsolete definition: < 24 hrs (should not be used anymore because tissue-death diagnosis is superior)
* localizable? if so to where?
* if symptoms not discretely localizable consider multifocal (cardioembolic?) or non-arterial-ischemia etiologies

## salience of working up and treating
* TIAs associated with significant increased short term risk of having a stroke
* TIAs represent an opportunity to avoid a stroke
* framing TIAs as an opportunity to avoid stroke may help convey important of evaluation to some patients who otherwise want to forgo this because they feel back to normal

## etiology of symptoms w/u
* calculate ABCD2 score assessment acuity: e.g. via MD calc at http://www.mdcalc.com/abcd2-score-tia/
    * note this should NOT be performed thoughtlessly and should be overridden depending on clinical scenario - subsequent studies have questioned its value
    * calculation (for score 0 to 7):
        * A: age >=60 (+1)
        * B: BP >= 140/90 (initial presenting BP, either systolic or diastolic above threshold) (+1)
        * C: clinical features
            * unilateral weakness (+2)
            * speech disturbance w/o weakness (+1)
            * other (0)
        * D: duration of symptoms
            * \>=60 min (+2)
            * \>=10 min (+1)
            * <10 min (0)
        * D: history of diabetes (+1)
    * correlation - increasing ABCD2 score associated with increased risk of stroke (following from Josephson et al papers- see refs):
        * ABCD2 vs. 90-day stroke risk:
            * 0 - 0%
            * 1 - 5%
            * 2 - 6%
            * 3 - 7%
            * 4 - 19%
            * 5 - 24%
            * 6 - 36%
            * 7 - 43%
        * ABCD2 vs. 2-day stroke risk:
            * low risk 0-3 score: 1.0%
            * moderate risk 4-5 score: 4.1%
            * high risk 6-7 score: 8.2%

* establish not stroke even if symptom remission - consider obtaining MRI as soon as possible
    * rationale:
        * could be stroke even if full symptom resolution
        * might acquire evidence of alternative process: e.g. non-stroke structural lesion such as demyelination, seizure generator/sequelae, neoplastic
## high risk lesions
* atherothrombotic lesions w > 70% stenosis at ICA origin
* intracranial atherothrombotic disease of posterior circulation producing low flow or emboli (distal vert, vert-basilar junction, prox basilar)
* proximal emboli to top of basilar or MCA stem
* dissections of: petrous ICA, C1-2 vertebral (as it enters foramen transversarium

## management - primary/secondary stroke prevention
* see general stroke workup - following is very succinct summary of that
* inpatient vs. outpatient
    * depends on clinically variables outside of scores
    * that being said, ABCD2 is associated w higher stroke risk, ergo consideration of hospitalization if w/i 72 hrs of event and one of following ABCD2 score/scenarios:
        * >=3
        * 0-2 with either:
            * uncertainty re ability to get diagnostic workup within 2 days as outpatient
            * other supporting evidence that event caused by focal ischemia
    * impetus behind management is to prevent strokes that TIAs may herald
* etiology w/u:
    * labs: S-TSH, A1c, fasting lipids
    * consider MCOT for a month if not clear etiology
    * TTE to evaluate for clot or thrombogenic cardio dysfunction
    * evaluate for vascular stenosis via consideration of one of following:
        * carotid US re anterior circulation symptoms
        * vascular imaging: MRA (gad with time of flight vs. just time of flight) or CTA
* treatment:
    * BP & glucose control if applicable
    * anti-thrombotic:
        * if not taking any: ASA 81
        * if on ASA 81: increase to ASA 325
        * if ASA allergy: consider plavix vs. desensitization
    * LDL target < 70 from initial LDL
<!-- get statin potency table here with algo for how to add/increase specific statins -->

## sources

* UpToDate:
    * Definition, etiology, and clinical manifestations of transient ischemic attack https://www.uptodate.com/contents/definition-etiology-and-clinical-manifestations-of-transient-ischemic-attack
    * Initial evaluation and management of transient ischemic attack and minor ischemic stroke https://www.uptodate.com/contents/initial-evaluation-and-management-of-transient-ischemic-attack-and-minor-ischemic-stroke
* Higher ABCD2 score predicts patients most likely to have true transient ischemic attack. Josephson SA et al. Stroke. 2008 Nov;39(11):3096-8. doi: 10.1161/STROKEAHA.108.514562. https://www.ncbi.nlm.nih.gov/pubmed/18688003
* Validation and refinement of scores to predict very early stroke risk after transient ischaemic attack. Johnston SC et al. Lancet. 2007 Jan 27;369(9558):283-92.

## further work

* mdcalc has some decent references worth mining: https://www.mdcalc.com/abcd2-score-tia#evidence
