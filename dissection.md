# dissection leading to stroke

## definition/mechanism

* dissection = separation in arterial wall layers resulting in blood seeping in = false lumen
    * from intimal tear, rupture, or vaso vasorum pathology

## location categories
* extracranial/cervical
    * anterior / carotid - mainly internal carotid artery (ICA)
        * ICA usually 1.5-3cm distal to carotid bifurcation to base of skull
    * posterior / vertebrobasilar - mainly vertebral artery (vert)
        * usually at C1-2 where mobile but tethered leaving transverse foramen & turns sharply to enter foramen magnum
* intracranial - less common, generally of MCA or basilar

## epidemiology
* extracranial dissections (much from Mayo Board review):
    * ~2% general population stroke, but ~10-25% strokes in younger than 45
    * ~50% with identifiable trauma / predisposing event
    * ICA: common age range of late 30s or early 40s with possible female predominance

## mechanisms / pathogenesis
* provocation: traumatic vs. spontaneous (may often be unclear)
* provocative factors / trauma
    * ICA + vert:
        * forceful/violent-coughing/Valsalva (e.g. childbirth), direct head/neck trauma (does not have to be severe), whiplash/neck-extension - event may be precede symptoms by days
    * vert:
        * most common: rapid & extreme rotation of neck (e.g. chiropractic manipulation, turning head driving)
        * contexts could include: bouts of coughing or trauma to neck/head. atlantoaxial dislocations where vert can be stretched/kinked through transverse processes or C1-2
* spontaneous pathogenesis
    * ICA - uncertain
        * in most cases cystic medial necrosis not found on microscopy
        * may be evidence of disorganized media & internal elastic lamina - unclear if specific
    * ICA + vert: rarely predisposing connective tissue condition as per below
* aortic arch dissection (traumatic or spontaneous) may result in dissection of multiple other vessels - may have chest/back pain, hypotension, BP difference between arms
* intracranial dissection
    * generally no associated trauma, though uncommonly minor head injury, violent-coughing/Valsalva, cocaine

## associated predisposing conditions - low percentage
* connective tissues diseases (SMALL number of patients) - confer increased dissection risk:
    * fibromuscular dysplasia (most common of this subset) - see stroke article
    * vascular/type-4 Ehlers-Danlos (<2% of all cases)
    * Marfan
    * osteogenesis imperfecta
    * transforming growth factor (TGF) B receptor mutation = Loeys-Dietz syndrome
    * pseudoxanthoma elasticum = Gronblad-Strandberg syndrome
* cystic medial necrosis, reticular fiber deficiency, homocystinuria, ADPKD, alpha-1 antitrypsin deficiency, segmental mediolytic arteriopathy, RCVS, cervical artery tortuosity, atherosclerosis
* atlantoaxial dislocations including odontoid hypoplasia
* with some/many of above association is tentative: may not be greater than expected for chance alone - caveat re suspicion as per workup section

## symptoms /signs
* vertebral
    * cervicooccipital pain & brainstem signs - variable w possibilities including
        * PICA blockage / lateral medullary syndrome
        * medial medullary syndrome
        * spinal artery occlusion: possible associated face-sparing hemi/quadri-plegia
        * subclavian steal if subclavian blockage proximal to L vert origin: L-arm-exercise-induced basilar insufficiency (w possible associated headache & arm claudication/pain)
        * proximal cervical vert occlusions may be compensated by anastomoses from other arteries: thyrocervical, deep cervical, occipital (or "reflux" from circle of Willis)
        * may be NO symptoms if on one side occlusion in vert proximal to PICA origin & retrograde flow via well-open/sized contralateral vert
        * may have fluctuations over minutes to hours
* internal carotid artery dissection
    * symptoms
        * may have unilateral head/face fluctuating achey pain days preceding stroke symptoms
            * this pain might be markedly steroid responsive
        * may have neck pain over dissection site
        * less common: amarousis fugax, lightheadedness, syncope, facial numbness
    * signs
        * Horner's ("painful Horner's usually due to underlying structural lesion" - Adam and Victor's)
        * less common: cervical bruit
        * possible cranial nerve involvement (close proximity / small branch feeders): vagus, spinal accessory, hypoglossal
* intracranial
    * fluctuating symptoms, ischemia relatable to vessel
    * severe unilateral pain: retroorbital for MCA, occipital for basilar, occipital/supraorbital for vertebral

## complications / associations
* ischemia as per above (TIA/stroke)
* dissections may extend from neck distally into cranial vessels
* vert: pseudoaneurysm
    * mostly intracranial type w associated risk of rupture leading to SAH
    * cervical type: low rate of associated rupture/ischemia
* rare association with RCVS (unclear etiologic relationship; vert > carotid)
* subset of intracranial dissections with subarachnoid hemorrhage

## diagnosis

* double lumen evident on CTA/MRI/MRA, possible detection of anterior dissection on carotid US (limited)
* "string sign" possible: variable-length irregular elongated narrow dye-column, w upper-end tapered occlusion or outpouching (Adams and Victor's)

## workup:
* given tentative associations and low proportion of patients found to have connective tissue or vascular disorder is low, additional testing not generally recommended unless specific higher suspicion
* factors that may increase clinical suspicion for connective/vascular disease as generator:
    * multiple extracranial vessel spontaneous dissection
    * widespread vascular toruosity
    * family history, joint hypermobility, multiple joint dislocations, translucent skin, poor wound healing, easy bruising, unusual scars

## treatment: non-definitive/varied
* extracranial:
    * acute management
        * tPA should NOT be withheld for otherwise eligible patients with associated acute ischemic stroke
        * reperfusion - endovascular re-opening of occluded vessel if rapid may be beneficial via angioplasty and/or stenting (risk of extending dissection)
    * subacute/chronic management
        * with associated acute ischemic neuro symptoms:
            * disagreement whether antiplatelet or anticoagulation therapy optimal, but some evidence of equivalent effectiveness - CADISS trial (via Wein et al. 2017)
            * if anticoagulation: LMWH or warfarin for ~6 wks to 3 months, with use of repeat imaging to guide duration (generally done even though not clearly evidence based; continuum 2/2017)
            * scenarios w relatively more support for anticoagulation (via Adams and Victor's) - non-definitive based on evidence
                * basilar artery thrombosis w fluctuating deficits
                * impending carotid artery occlusion from thrombosis or dissection
            * if carotid occlusion - "role of anticoagulation is less clear" (Adams and Victor's)
        * without associated acute ischemic neuro symptoms
            * antiplatelet therapy
        * if anticoagulation is used for non-occlusive dissection:
            * generally discontinuation months to ~1 yr when repeat angiography demonstrates patent lumen or >=50% normal diameter w smooth wall
* intracranial
    * lack of evidence for anticoagulation
    * antiplatelet therapy
* consider steroids for pain

## prognosis with intracranial/extracranial dissection
  * "complete or excellent recovery" in ~70-85%
  * disabling deficits ~10-25%, death ~5-10%
  * however, if complicated by stroke: ~25% mortality & most others w significant impairment
  * reported recurrence rates broad, but ischemia specifically from recurrent dissection thought low
      * extracranial dissection per CADISS trial: low frequency recurrent events ~1-3%
      * recurrent ischemic symptoms (not clearly specifically from dissection) ~0 to 13% (uptodate)

## refs
* Adams and Victor's
* Mayo Board Review
* continuum 2/2017
* uptodate:
    * Spontaneous cerebral and cervical artery dissection: Clinical features and diagnosis
    * Spontaneous cerebral and cervical artery dissection: Treatment and prognosis
* "Canadian stroke best practice recommendations: Secondary prevention of stroke, sixth edition practice guidelines, update 2017." Wein et al. International journal of stroke, 2017. PMID: 29171361. DOI: 10.1177/1747493017743062
