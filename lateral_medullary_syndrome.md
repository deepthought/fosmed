# lateral medullary syndrome

* common stroke syndrome due to lesion in posterolateral medulla
* aka Wallenberg Syndrome

## common symptoms/signs & affected nuclei/tracts

| symptoms/signs | nuclei/tracts |
|---:|----|
| contralateral body pain/temperature sensation loss |  lateral spinothalamic tract |
| ipsilateral face pain/temperature sensation loss/paresthesias |  spinal trigeminal nucleus & tract (CN5) |
| ipsilateral Horner's syndrome | descending sympathetic fibers |
| ipsilateral hemiataxia | inferior cerebellar peduncle |
| ipsilateral laryngeal/pharygneal/palatal muscle weakness (dysphagia/hoarse)| nucleus ambiguus or CN9/10|
| vestibular syndrome (vertigo/vomiting/nystagmus) | vestibular nuclei (CN8) |

![MRI](flosmed_images/WallenbergInfarct_via-wiki.jpg)

## clarifications / more details

* note that partial/fragmentary syndromes more frequent than presentation with prototypical syndrome
    * vertigo most common feature
    * might have only lateropulsion & mild ipsilateral limb ataxia from small infarct (Adams and Victor's)


* sympathetic pathways
    * descending in lateral reticular formation
    * run in lateral tegmentum of brainstem near spinothalamic tracts
    * associated Horner's symptoms: ptosis & miosis, less commmonly anhidrosis


* nucleus ambiguus gives rise to:
    * CN9 glossopharyngeal nerve - stylopharyngeal muscle
    * CN10 vagus nerve branchial efferent motor fibers - largyneal/pharngeal muscles
    * associated symptoms: dysphagia, dysarthria/hoarseness
    * associated signs: paralysis of ipsilateral palatal elevators, pharngeal constrictors, vocal cord decreased gag reflex


* cerebellar hemisphere / spinocerebellar features:
    * anatomical regions: olivocerebellar, spinocerebellar, restiform body (lateral subdivision of inf. cerebellar peduncle at dorsolateral medulla), and inferior cerebellum
    * ataxia / dysmetria / intention tremor / hypophonia
    * oscillopsia
    * falling to ipsilateral side / sensation of lateropulsion


* note may have overlap in symptoms with vestibular nuclei & cerebellar pathways: oscillopsia / imbalance / nausea/vomiting (attribution per Adams/Victor's - vestibular nuclei, Brazis - cerebellar)


* note re why facial sensation deficits are usually contralateral
    * face pain/temp sensation received by first order trigeminal neurons in trigeminal ganglion
    * the first order trigeminal neurons enter lateral pons and descend in spinal trigeminal tract (analogous to posterolateral/Lissauer's tract = spinal cord ascending & descending axons carrying pain/temp from body a few levels before crossing)
    * spinal trigeminal tract descends in posterolateral aspect of pons/medulla - thus affected in lateral medullary syndrome
    * first order neurons synapse in spinal trigeminal nucleus in medulla/pons (nucleus extends even to upper cervical spine) - this nucleus is rostral extension of spinal cord dorsal horn
    * second order neurons ascend in the anterior trigeminothalamic tract to thalamus - not affected in lateral medullary syndrome due to more medial/anterior localization


* note facial sensory abnormalities may include painful/burning paresthesias in addition to impaired pain/temperature sensation

## additional regions / symptoms / signs

* other regions that may be involved:
    * central tegmental tract may be involved - connects inferior olive to red nucleus
        * palatal myoclonus may be observed
    * nucleus solitarius may be involved (anterior to vestibular nuclei)
        * decreased ipsilateral taste sensation


* hiccups may be observed
    * reflex arc thought:
        * afferent: phrenic, vagus, dorsal sympathetics
        * efferent: phrenic
    * potential localizations:
        * CN9/10 / nucleus ambiguous
        * reticular formation & vestibular nuclei (Brazis)


* varied eye signs - may have fragments of (see Adams & Victor's):
    * skew deviation (affected eye higher)
        * i.e. acquired vertical eye misalignment due to supranuclear dysfunction (brainstem, cerebellum, or inner ear), typically constant in all gaze positions
    * direction changing nystagmus
    * internuclear ophthalmoplegia (presumably with medial infarct extension as MLF should be more medial)


## localizing features / syndrome overlap

* rarely severe vertical disorientation associated w vestibular nuclei dysfunction
    * e.g. feeling as if whole world upside-down or sideways
    * e.g. tilting of vision & rotation of vertical meridian


* medulla-specific localizing features (e.g. over pontine syndrome):
    * hoarseness (nucleus ambiguus)
    * loss of taste sensation (nucleus solitarius)


* syndromes sharing some features:
    * AICA syndrome (ipsilateral hearing loss suggests AICA involvement rather than lateral medullary syndrome)
    * SCA syndrome

## exceptions to prototypical symptoms/signs

* in some cases facial involvement is contralateral - proposed reasons:
    * involvement of crossing fibers
    * stronger/enhanced sensation in affected face due to paresthesias (particularly early in course)
* while generally no associated weakness of face/body, rarely this may be observed
    * ipsilateral facial weakness - from fibers of facial nerve that loop caudally into medulla prior to exiting at pontomedullary junction
    * contralateral body weakness - if medial extension of infarct
* less commonly body sensation deficits ipsilateral per Adams & Victor's
    * involvement of cuneate/gracile nuclei - ipsilateral limb numbness

## etiology

* almost always infarction
    * ~80% cases from vertebral artery via atherothrombosis
    * other arteries affected: PICA or lateral medullar arteries (branches off vertebral superior to PICA)
        * if PICA embolism may have isolated inferior cerebellar infarct with embolus travel distal to PICA origin
    * note thrombotic is more common than embolic
* small percentage cases from bleed, demyelination, or tumor

## complications

* sudden respiratory/cardiac death after initial recovery (in absence of significant swelling or clear arterial occlusion)
* airway/swallowing control in context of dysphagia from CN 9/10 dysfunction

## useful images

* medulla cross-section with nuclei:
    * https://classconnection.s3.amazonaws.com/624/flashcards/1292624/png/upper_medulla1331177159136.png
    * google image search: https://www.google.com/search?q=medulla+cross+section+solitary+nucleus&tbm=isch
    * see Adams and Victor's Lateral Medullary Syndrome section - figure of transverse section through upper medulla

* general note on diagrams vs. MRI scans
    * MRIs presented generally with anterior at top
    * diagrams presented generally with posterior at top
    * if you are an educator presenting/labeling diagrams/scans: please label anterior/posterior axes!

## free and open source case reports

* [sudden onset headache and gait ataxia](http://www.jusurgery.com/universalsurgery/lateral-medullary-syndrome-case-report.pdf) - doi: 10.21767/2254-6758.100067 (creative commons license)
* [hiccups as sole presenting feature](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4100430/) - doi:  10.4103/0253-7176.135397 (creative commons license)

## sources
* Adams and Victor's 10th Ed.
* Blumenfeld
* Neurologic Examination by Biller, Gruener, Brazis
* MRI image from John S. To on wikipedia (public domain): https://en.wikipedia.org/wiki/File:WallenbergInfarct001.jpg
