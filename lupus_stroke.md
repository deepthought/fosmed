# lupus and stroke

## potential lupus ischemic stroke mechanisms (with overlap)

* thrombosis / thromboembolism / coagulopathy
    * antiphospholipid syndrome
    * arterial & venous thrombosis
* vasculopathy
    * premature atherosclerosis (diff including steroid therapy)
    * small vessel vasculopathy
    * vasculitis (not common)
    * vessel spasm
* dissection

* recurrent small infarcts = most common neurologic manifestations with antiphospholipids

## contributing stroke risk factors

* hypertension - renal disease and steroid treatments
* valvular heart disease
* infections (e.g. relating to immunosuppressive therapies)

## antiphospholipid antibodies (aPL)

* associated mainly with MRI small hyperintense white matter lesions consistent with vasculopathy
* more prevalent in CNS lupus (stroke / TIA / seizure) vs. lupus without CNS involvement (~55% vs. 20% per uptodate)
* antibodies:
    * lupus anticoagulant (LA) - arterial & venous thrombosis. interferes with phospholipid-dependent coagulation tests. components can include
        * dilute Russells viper venom test
        * sensitive activated thromboplastin time (aPTT)
        * mixing test with pooled normal plasma
        * confirmatory test with increased phospholipid
    * anticardiolipin (aCL)
    * anti-B2-glycoprotein (aB2GPI) - associated w better predictive values re stroke (Hawro et al.)
    * anti-phosphatidylserine/prothrombin (aPS/PT)

## antiphospholipid syndrome diagnosis (mainly from uptodate table)

* need at least one clinical criterion & one lab criterion
* clinical criteria:
    * venous or arterial thrombosis
    * pregnancy complications:
        * unexplained death of morphologically normal fetus >= 10 wks gestation
        * premature birth of morphologically normal neonate before 34 wks due to
            * eclampsia or severe pre-eclampsia
            * placental insufficiency
        * three unexplained consecutive spontaneous abortions before 10th wk of gestation
            * excluding abnormal parental chromosomes or maternal anatomic/hormonal abnormalities
* lab criteria - all need to be on at least 2 occasions at least 12 weeks apart
    * LA
    * aCL: IgG or IgM titer > 99th percentile
    * aB2GPI: titer > 99th percentile

## other studies

* serologies (derived mainly from uptodate diagnostics page) to consider
    * antibody panoply:
      * ANA positive in most SLE but non-specific
      * initial wave of tests to consider in potential lupus with positive ANA:
          * more specific for SLE:
              * anti-dsDNA (in ~70%), anti-Sm (in ~30%)
              * antiroibosomal P protein (low sensitivity)
          * anti-Ro/SSA (in ~30%) & anti-La/SSB (in ~20%) - though more commonly associated with Sjogren's
          * anti-U1 RNP (in ~25%) - also non-specific. almost always seen in mixed connective tissue disease
      * perform in selected patients depending on other antibody screens
          * Rheumatoid factor (RF; in ~20-30% SLE) & anti-cyclic citrullinated peptide (CCP; higher specificity for RA)
    * infectious studies if short symptom duration (e.g. < 6 weeks):
        * parvovirus B19
        * hepatitis B / C
        * lyme
        * EBV
    * elevated homocysteine associated with increased stroke risk and other thrombotic events
* CSF findings associated with neuropsychiatric lupus - elevated:
    * aCL, LA, antiphospholipid, antiribosomal P, antineuronal antibodies

## re watershed infarcts

* two main types: cortical vs. internal watershed (http://stroke.ahajournals.org/content/36/3/567)
* internal:
    * along & slightly above lateral ventricle
    * between deep & superficial arteries of MCA
    * between superficial MCA & ACA systems
* cortical: between cortical ACA/MCA/PCA border zones
* internal borderzone more associated with hypoperfusion in lupus ([see Kaichi et al.](http://www.ajnr.org/content/ajnr/early/2013/07/25/ajnr.A3645.full.pdf))
    * possible mechs include: vasculitis or arterial vasospasm

## treatments

* consideration of immunosuppressive induction therapy
    * e.g. methylprednisolone 1mg/kg/day for 3 days (or 1g/day for acutely ill)

## refs

* "Intractable Headaches, Ischemic Stroke, and Seizures Are Linked to the Presence of Anti-β2GPI Antibodies in Patients with Systemic Lupus Erythematosus". Hawro et al. 2015. PMID 25781014. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4362944/
* "Stroke subtypes among young patients with systemic lupus erythematosus". 2005. PMID 16378793. http://www.amjmed.com/article/S0002-9343(05)00497-3/pdf
* "Acute occlusive large vessel disease leading to fatal stroke in a patient with systemic lupus erythematosus: arteritis or atherosclerosis?" 2006. PMID 16508973. http://onlinelibrary.wiley.com/doi/10.1002/art.21656/full
* uptodate:
    * Neurologic manifestations of systemic lupus erythematosus
    * Diagnosis and differential diagnosis of systemic lupus erythematosus in adults
    * Overview of the management and prognosis of systemic lupus erythematosus in adults
* "Brain MR Findings in Patients with Systemic Lupus Erythematosus with and without Antiphospholipid Antibody Syndrome". Kaichi etl a.. 2014. http://www.ajnr.org/content/ajnr/early/2013/07/25/ajnr.A3645.full.pdf
* guidelines for lupus anticoagulant detection. PMID 19624461 http://onlinelibrary.wiley.com/doi/10.1111/j.1538-7836.2009.03555.x/pdf
* Mayo Clinic Medical Laboratories: lupus anticoagulant profile. http://www.mayomedicallaboratories.com/test-catalog/Clinical+and+Interpretive/83092
