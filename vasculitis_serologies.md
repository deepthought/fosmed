# vasculitis serologies
### to be considered in neurovasculitides / cerebral vasculitis

* basic studies: CBC, renal panel, LFTs, urinalysis, urine drug screen
* general inflammatory markers:
    * ESR
    * CRP
* ANA
* antibodies to: Ro/SS-A, La/SS-B, Sm, RNP, dsDNA, Scl70, centromere
* antiphospholipid antibodies:
    * lupus anticoagulant (LA)
    * anticardiolipin (aCL) IgG & IgM
    * anti-beta2-glycoprotein 1 IgG & IgM
* serum complement levels: C3, C4
* quantitative immunoglobulin levels (IgG, IgM, IgA) including IgG4
* ANCA: p-ANCA/MPO, c-ANCA/Pr-3
* viral hepatitis panel
* HIV
* VZV
* serum cryoglobulins
* serum and urine protein electrophoresis w immune electrophoresis (sometimes called "MPSS" for monoclonal protein serum study)

## sources include

* UpToDate including:
    * Overview of and approach to the vasculitides in adults
    * Primary angiitis of the central nervous system in adults
* Neurovasculitis, Younger, Continuum 2015
* Rheumatology and Neurology, Dimberg, Continuum 2017
