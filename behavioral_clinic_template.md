##### behavioral neurology oupatient clinic template v1

## HPI
name:  
age:  
handedness:  
CC referral:  
CC patient:  

symptom onset:  
course (progressive?):  
additional HPI:  

&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;
&nbsp;   

### ROS
* **memory**  
misplacing things? change?  
disorientation? repeated questions?  
finances: doing now? previously did?  
stove-on/water-running:
driving (yes/no, accidents, lost):  

* **other cognitive function**   
language-dysfunction: dysarthria, non-sense words/speech, anomia
apraxia: trouble using e.g. doorknobs or utensils?
visuospatial: lost navigating familiar places?

* **sleep**   
RBD: acting-out-dreams, punching/kicking in sleep, falling out of bed
insomnia
snoring

* **gait**  
falls &nbsp; &nbsp; &nbsp; near-falls &nbsp; &nbsp; &nbsp; imbalance &nbsp; &nbsp; &nbsp; changes

* **autonomic symptoms**  
dizziness/lightheaded/orthostasis &nbsp; &nbsp; &nbsp; syncope &nbsp; &nbsp; &nbsp; bowel/bladder-problems/changes &nbsp; &nbsp; &nbsp; fevers/chills/sweats &nbsp; &nbsp; &nbsp; palpitations

* **ADLs**  
basic: cooking &nbsp; &nbsp; &nbsp; toileting &nbsp; &nbsp; &nbsp; feeding &nbsp; &nbsp; &nbsp; dressing
instrumental: finances/checkbook &nbsp; &nbsp; &nbsp; keeping appointments

* **alt-neuro**  
weakness &nbsp; &nbsp; &nbsp; numbness/tingling

* **psych**  
mood:  
depressed &nbsp; &nbsp; &nbsp; anxious &nbsp; &nbsp; &nbsp; hallucinations  
stressors:

## PMHx
stroke &nbsp; &nbsp; &nbsp; HTN &nbsp; &nbsp; &nbsp; DM &nbsp; &nbsp; &nbsp; HLD &nbsp; &nbsp; &nbsp; a-fib/heart-dz &nbsp; &nbsp; &nbsp; OSA
migraine &nbsp; &nbsp; &nbsp; seizure &nbsp; &nbsp; &nbsp; head-trauma &nbsp; &nbsp; &nbsp; meningitis &nbsp; &nbsp; &nbsp;

&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;  

## meds
&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;  

## SHx
EtOH:  
tobacco:  
illicit drugs:  
exercise:
occupation:  
education level:  
married/prior-marriages:  
living location/situation:  

## FHx
dad:  
mom:  
siblings:  
children:  
other dementia:  

## Exam
MMSE:

CV:  
Resp:  
Ophtho:  

Neuro:  
MS:  
CN:  
Mot:  
Sens:  
Refl:  
Coord:  
Gait:  

## Labs/Studies
MRI:  
neuropsych:  
LP:  

## Impression
Formulation:  
*[age]* year-old *[R/L]*-handed *[man/woman]* presents with *[chief complaint]*. Exam notable for *[notable exam elements]*. Studies notable  for *[notable studies]*. Localization includes: *[localization differential if applicable from cortex to muscle vs. e.g. diffuse]*.

Differential includes:  

* dementia: cognitive complaint of sufficient severity to compromise daily functioning  
* MCI: cognitive complaint, abnormal for age, cognitive decline/change, essentially normal functional/day-to-day activities  
 * if MCI/dementia: amnestic or non-amnestic?
* neurodegenerative: slow/gradual course  
     * alzheimer's: can include atypical presentations affecting attention, concentration, and language  
     * dementia with lewy bodies: attention, concentration, visuospatial difficulty  
     * frontotemporal dementia: behavioral changes with inappropriateness, apathy, lack of insight, & attentional/concentration difficulty  
* vascular: risk factors? acute changes possible  
* psychiatric: depression/anxiety?  
* general medical: CHF, DM, COPD  

## Plan

labs: CBC &nbsp; &nbsp; &nbsp; renal &nbsp; &nbsp; &nbsp; LFTs &nbsp; &nbsp; &nbsp; S-TSH  &nbsp; &nbsp; &nbsp;  B12/MMA / folate &nbsp; &nbsp; &nbsp; thiamine &nbsp; &nbsp; &nbsp; RPR (AAN: VDRL non-routine, only if "clinically indicated")

EKG (consider if e.g. anticholinesterase inhibitor)

MR-brain &nbsp; &nbsp; &nbsp; CT-head (consider neuroimaging if not done in last yr)

memory education &nbsp; &nbsp; &nbsp; brain-tips sheet

neuropsych (consider if MMSE > 15 & not done within last yr)

psych (if e/o depression)

sleep study (consider if snoring, non-restorative sleep, RBD symptoms)

rtnv (time frame?)?  

##### copyright CC-BY4
