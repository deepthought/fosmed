# brain attack - acute ischemic stroke management

## overview
1.  rapidly assess - every minute that goes by pre-treatment could correlate to ~2 million neurons lost & potentially difference for longer term favorable outcome
2.  give tPA rapidly if indicated
3.  perform endovascular intervention rapidly if indicated

## rapid data aquisition
### immediate impression
* pre-arrival - acquire as much data as possible - e.g. phone number of patient/family/witnesses or patient name/ID/medical-record-number
* if patient arriving via EMS and facility can get weight on stretcher - immediately get weight re possible tPA dosing
* if looks like large vessel syndrome -> activate endovascular team for possible clot retrieval (if applicable at institution)

### rapid HPI
* last known normal
    * VERIFY - w caveats/pitfalls:
        * source of information: eyewitness? relay (nursing home to EMS?)
        * distrust relative timing ("an hour ago") given "hour ago" may be repeated e.g. 30 min later
        * look for objective information - phone call/text times, receipts, contact information for any eye witnesses or family
    * do EVERYTHING you can to RAPIDLY acquire this information - e.g. call any available witness/family numbers
* any medical problems
* any recent problems / hospitalizations / procedures
* interrupt history-giver if tangential history re "time is brain" - most are understanding
* on any blood thinners (e.g. oral or heparin) or on dialysis
* occupation (framing what might be disabling deficit)

### rapid exam / data
* NIHSS
* bood pressure
* non-contrast head CT
* glucose level
* weight

### neuroimaging
* consider asking tPA be mixed while getting CT if would given in absence of bleed/evolving-hypodensity
* CT analysis
    * ASPECTS scoring - consider for anterior circulation (subtract from score of 10)
        * score <= 7 predicts: worse functional outcome at 3 months & symptomatic hemorrhage, and possibly poor outcome with tPA
        * score >=6 associated w higher likelihood of favorable outcome w endovascular intervention (see endovascular section)
        * regions
            * caudate
            * putamen
            * internal capsule
            * insular cortex
            * M regions at basal ganglia level
                * M1: anterior MCA - frontal operculum
                * M2: MCA lateral to insular ribbon - anterior temporal lobe
                * M3: posterior MCA - posterior temporal lobe
            * M regions at ventricle level just above basal ganglia
                * M4 - just above M1
                * M5 - just above M2
                * M6 - just above M3
    * extensive obvious hypodensity - indicative of irreversible brain injury such that tPA would probable worsen than help patient
    * minor/early ischemic change not contraindication (hence ASPECTS)
* consider vascular imaging when suspect large vessel occlusion
    * eg. gaze preference, neglect > hemiplegia

## tPA consideration

### inclusion criteria

* clinical diagnosis of ischemic stroke causing neurologic deficit
* onset < 4.5 hours
* age >= 18

### exclusion criteria

* history (generally absolute contraindications unless otherwise stated)
    * brain insults
        * severe head trauma OR ischemic stroke within w/i 3 months
        * intracranial lesion - cancer, AVM, or aneurysm (i.e. that could bleed)
    * bleeding:
        * prior intracranial hemorrhage
        * active internal bleeding
        * bleeding diathesis (i.e. a known coagulopathy)
    * medical history RELATIVE contraindications
        * acute MI w/i 3 months
            * give if concurrent MI/stroke
            * consider if after recent MI unless STEMI involving L anterior myocardium
        * pregnancy - consider in moderate-severe re benefit/risk
        * seizure at onset w postictal residual deficits
            * consider if residual deficits thought caused by stroke
    * recent procedures/trauma:
        * CNS surgery w/i 3 months: intracranial or intraspinal
        * arterial puncture at non-compressible site w/i 1 wk
        * RELATIVE contraindication:
            * major extracranial trauma OR major surgery w/i 2 wks - can still consider
            * GI or GU surgery w/i 3 wks - consider if no structural bleeding lesions
* meds
    * DTI or XaI in 48 hrs unless nl coag tests (direct thrombin inhibitor or factor Xa inhibitor)
        * w/i 24 hrs
        * w/i 48 hrs with abnormal coags
            * e.g. via aPTT, INR, ECT (ecarin clotting time), TT, or Xa activity (if applicable)
        * factor Xa inhibitors include: rivaroxaban (xarelto), apixaban (eliquis)
        * direct thrombin inhibitors include: dabigatran (pradaxa/prazaxa)
    * heparin:
        * w/i 24 hours
        * w/i 48 hrs with abnormally elevated aPTT
    * warfarin (coumadin) if INR > 1.7
* vitals
    * BP > 185/110
* exam:
    * RELATIVE contraindication:
        * "minor stroke" NIHSS < 5 (consider if disabling symptoms, particularly if w/i 3 hrs)
        * rapidly improving symptoms/signs - still consider given despite improvement, if a disabling degree of deficit present
    * for extended window 3-4.5 hrs: NIHSS > 25 - risk/benefit uncertain
* labs
    * glc < 50
        * reconsider if deficits persist after glucose normalization
    * plts < 100k
    * INR > 1.7
    * PT <15
* neuroimaging
    * CT showing hypodensity > 1/3 of cerebral hemisphere

### stroke mimics - generally treat
* "accumulating data suggest that patients with stroke mimics who are treated with alteplase have benign outcomes" (UpToDate)

### other extending time window considerations - i.e. 3-4.5 hrs (modified ~2017)
* other factors not seemingly different
    * age > 80 - tPA recommended
    * warfarin use - recommended if INR < 1.7
    * prior stroke or DM - "probably recommended"
    * (prior factors included NIHSS>25)
* suspected SAH
* tPA administration
    * if eligible -> consent: patient, proxy, or implicit (situation)
    * dosing:
        * 0.9 mg/kg with max dose of 90mg, with 10% given as initial bolus
    * benefit/risk ratio and discussion with patients/family:
        * 6% increased risk of symptomatic intracranial hemorrhage
        * overall absolute 13% lower rate of significant disability (based on modified rankin score of 0 or 1)
* consider ASA 325 administration if no tPA is administered
* endovascular intervention
    * consider in tPA ineligible patients with significant deficit if within 6 hrs
        * consider CTA/P for evidence of retrievable clot
        * time window may be extended in future pending ongoing studies and institution

### labs pre-tPA
* fingerstick glucose - this is ONLY lab to necessarily wait for "unless expect other abnormalities"
* serologies (most physicians will want these prior to tPA administration, thought not always required)
    * glucose
    * coags including PTT, PT/INR - only required if recent anticoagulant use
    * CBC w diff
    * BhCG if woman of childbearing age

### tPA consent / talking to families

* define ischemic stroke: interruption in blood flow to the brain causing death of brain cells, often due to a clot
* tPA basics & probabilities above:
    * if you are recommending tPA then be explicit: "I/we are recommending this"
    * be explicit about why: "wouldn't recommend this unless we thought change of helping outweighed the chance of hurting"
    * explicit about GENERAL change of helping: "~13% treated will have significantly reduced disability from stroke"
    * explicit about GENERAL BIG risk: "biggest risk is chance of bleeding, with ~6% chance of having a significant bleed"
    * generally discussion of bleed can be terrifying, so repetition of why you would risk this is good: "but I/we wouldn't be recommending this if we didn't think the chance of helping outweighed risk of harm"

### IV tPA dosing
* 0.9mg/kg to maximum of 90mg
    * first 10% administered as IV bolus over 1 minute
    * remaining 90% infused over 1 hour
* Japan alternate dosing: 0.6mg/kg is approved dose, but in ENCHANTED trial this dose vs. 0.9mg/kg failed to meet non-inferiority criteria

### tPA stats
* Number needed to treat for tPA to prevent 1 death or significant disability (per continuum article - note there are good pictographs re easy to understand % breakdown):
    * NNT is 8 when within 3 hours
    * NNT is 14 when within 3 to 4.5 hours

* Within first 3 hours onset (per Continuum article):
    * ~ 6% increased risk of symptomatic intracranial hemorrhage
    * ~13% lower rate of significant disability (mRankin of 0 or 1)
    * improve odds of favorable outcome from 1 in 5 to 1 in 3 (need to verify)

* benefit is operationalized as functional outcome at 3-6 months

* time is brain (Saver, 2005):
>Every minute in which a large vessel ischemic stroke is untreated, the average patient loses 1.9 million neurons, 13.8 billion synapses, and 12 km (7 miles) of axonal fibers. Each hour in which treatment fails to occur, the brain loses as many neuron as it does in almost 3.6 years of normal aging.

### tPA complications to observe for include:
* significant/symptomatic ICH in about 6%
    * clinical trial 5-7%
    * observational prospective studies: CASES 4.6%, STARS 3.3%, SITS-MOST (NINDS definition) 7.4%
    * definition/operationalization of symptomatic ICH:
    * NINDS: any hemorrhagic transformation temporally related to neurologic worsening)
    * ECASS 2 & SITS-MOST: associated w "substantial" clinical worsening >=4 NIHSS points - reported more predictive of adverse long-term outcome
* systemic bleeding
* orolingual/angio-edema: 1-8%, typically mild, transient, and contralat to ischemic hemisphere
    * rarely severe re partial airway obstruction requiring emergent airway management - CT tongue can eval angioedema from hematoma. can consider antihistamines/steroids vs. intubation (e.g. if stridor)
* re intracranial hemorrhage / hemorrhagic conversion
    * consider if sudden/severe headache, worsening deficit, or reduced level of alertness
    * if suspect immediately:
        * stop tPA infusion (if running)
        * get labs: coags (PTT, PT/INR, CBC, fibrinogen level, blood type/cross-match
        * repeat head CT
    * if ICH confirmed:
        * alert or consult neurosurgery re potential need for evacuation/decompression
        * admin 6-8 IV cryoprecipitate, 6-8 units of platelets
        * consider admin 40-80 mg/kg recombinant activated factor VIIa (while waiting for cryo/platelet response)

## endovascular intervention

* mechanical thrombectomy - evidence of benefit (grade 1A) if:
    * inclusion:
        * large artery occlusion causing proximal anterior circulation ischemic stroke
        * 2nd generation retriever device can be used
        * NIHSS >=2
        * ASPECTS >=6
        * no ICH on imaging
        * w/i 6 hrs onset at artery puncture
        * informed consent
        * age >= 18 yo
    * exclusion:
        * BP > 185/110
        * glc < 2.7 or > 22.2 mmol/L
        * \> 0.9mg/kg or > 0.9mg tPA given
        * plt < 40k
        * INR > 3.0
    * regardless of whether tPA administered
* in practice endovascular intervention is considered outside of the above based on institution, imaging findings, specific intervention types available
    * intra-arterial tPA (IAtPA) is another consideration
    * basilar artery occlusion - not covered in above but based on operator/institution thrombectomy or IAtPA might be considered
    * some may consider up to 12 hours if evidence of CTA/P flow/volume mismatch - though evidence arguably murky

## emerging treatment areas

* treating wake-up strokes (i.e. where last known normal >4.5 hrs) - might consider treating if no early ischemic changes on CT (not standard of care/guidelines)
* ultrasound as a potential therapy - unproven

## references

* CVD continuum 2017
* CVD continuum 2015 including:
    * "Evaluation and Management of Acute Ischemic Stroke." Khatri, Pooja MD, MSc. Continuum. DOI: doi: 10.1212/01.CON.0000446101.44302.47.
* UpToDate including:
    * "Intravenous fibrinolytic (thrombolytic) therapy in acute ischemic stroke: Therapeutic use"
    * "Reperfusion therapy for acute ischemic stroke"
* "Time is Brain" by Jeffrey Saver, 2005. http://stroke.ahajournals.org/content/37/1/263
* Micromedex: "ALTEPLASE, RECOMBINANT"
* ASPECTS:
    * http://www.aspectsinstroke.com/
    * see CVD continuum 2017 "Treatment of Acute Ischemic Stroke" page 13
    * https://radiopaedia.org/articles/alberta-stroke-program-early-ct-score-1
* Mayo Board Review Book
* in-hospital observation

## stroke trials

* [clinicaltrials.gov](https://clinicaltrials.gov/ct2/results?term=acute+stroke+management&type=&rslt=&recrs=g&recrs=h&recrs=e&age_v=&gndr=&cond=&intr=NOT+awareness&titles=&outc=&spons=&lead=&id=&cntry1=&state1=&cntry2=&state2=&cntry3=&state3=&locn=&phase=0&phase=1&phase=2&phase=3&rcv_s=&rcv_e=&lup_s=&lup_e=#wrapper)
