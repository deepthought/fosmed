##### headache neurology outpatient template v1

## HPI
name:  
age:  
handedness:  
CC referral:  
CC patient:  

how many types of headaches?


### headache characterization

headache #____

* onset
* location
* severity (now, maximum, average) & ?impact on ADLs/work?
* quality
* course/evolution (e.g. time to maximal intensity, remits?)
* exacerbating factors
    * light
    * sound
    * smells
    * position (standing vs. supine)
* triggering factors:
    * sleep, diet/foods, weather
    * valsalva / sneezing / cough / bowel or bladder movements
    * for women: menstruation
* associated symptoms
    * nausea / vomiting
    * vision changes (bright or dark spots or vision loss before/during?)
    * focal sensorimotor symptoms or other focal neuro deficits
    * vasomotor symptoms (running nose, watery eyes, red eye)
* alleviating factors
* remember exact day/time headache started?
* waking up at night because of headache

(repeat above for each headache)

### modifying factors re all headaches

* caffeine intake (specifically ask re coffe, tea, soda, bc powder)
* sleep
* fluid intake
* diet/skipping meals
* stressful life events / changes

### medicines for headaches or other pain

* for each med: dose, frequency, start of use, evolution/course of use, benefit, side-effects?

#### current acute-tx/abortive medicines
&nbsp;  

#### current prophylactic-tx medicines
&nbsp;  

#### prior acute-tx/abortive medicines
&nbsp;  

#### prior prophylactic-tx medicines
&nbsp;  

### prior headache workup

* prior diagnoses relating to headaches (e.g. migraine, tension headache?)
* ever head neuroimaging: MRI (?contrast) or CT


### ROS

* motion sickness
* dizziness / vertigo
* snoring / apneic events at night

## PMHx
migraine diagnosis &nbsp; &nbsp; &nbsp;
sleep apnea &nbsp; &nbsp; &nbsp;
psychiatric comorbidities (depression / anxiety ) &nbsp; &nbsp; &nbsp;
stroke &nbsp; &nbsp; &nbsp;
seizure &nbsp; &nbsp; &nbsp;
head-trauma &nbsp; &nbsp; &nbsp;
meningitis &nbsp; &nbsp; &nbsp;
HTN &nbsp; &nbsp; &nbsp;
DM &nbsp; &nbsp; &nbsp;
HLD &nbsp; &nbsp; &nbsp;
a-fib/heart-dz

&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;  

## meds
&nbsp;  
&nbsp;  
&nbsp;  
&nbsp;  

## SHx
EtOH:  
tobacco:  
illicit drugs:  
occupation:  
married/prior-marriages:  
living location/situation:  

## FHx
headaches
specific headache d/o dx by neurologist/MD (e.g. migraine)
dad:  
mom:  
siblings:  
children:  

## Exam
weight / BMI:  
CV:  
Resp:  
Ophtho:  

Neuro:  
MS:  
CN:  
Mot:  
Sens:  
Refl:  
Coord:  
Gait:  

## Labs/Studies
MRI:  

## Impression
Formulation:  
*[age]* year-old *[R/L]*-handed *[man/woman]* presents with *[chief complaint]*. Exam notable for *[notable exam elements]*. Studies notable  for *[notable studies]*. Localization includes: *[localization differential if applicable from cortex to muscle vs. e.g. diffuse]*.

Differential includes:  

### primary headache disorders

#### migraine

* criteria for w/o aura:
    * at least 5 episodes meeting the following criteria, w/o another better diagnostic etiology
    * duration of 4-72 hours (untreated or unsuccessfully treated)
    * 2 of the following characteristics:
        * unilateral
        * pulsating
        * moderate to severe pain intensity
        * aggravated by, or causing avoidance of, routine physical activity
    * during headache one of the following:
        * nausea or vomiting
        * photophobia and phonophobia

* criteria for w aura:
    * at least 2 episodes meeting following criteria, w/o another better diagnostic etiology & with exclusion of TIA
    * at least one of the following fully-reversible aura symptoms
        * visual
        * sensory
        * speech and/or language
        * motor
        * brainstem
        * retinal
    * for qualification of TYPICAL aura: fully reversible visual, sensory, and/or speech/language symptoms - without motor, brainstem, or retinal symptoms
    * 2 of following:
        * one aura symptom spreads gradually over >=5 min OR two or more symptoms occurring in succession
        * each individual aura lasting 5-60 min
        * at least one aura symptom unilateral
        * headache accompanying aura OR following within 60 min        

#### tension

* criteria require the following
    * 2 of:
        * bilateral location
        * quality pressing/tightening (non-pulsating)
        * mild/moderate intensity
        * not aggravated by routine physical activity (e.g. walking or stair-climbing)
    * both of:
        * no nausea or vomiting
        * no more than one of photophobia or phonophobia

* episodic type: at least 10 episodes lasting 30min to 7days
* chronic type: hours to days or unremitting, >=15 days/month on average for more than three months (>=180 days per year)

#### cluster

#### new daily persistent headache

* dx criteria:
    * distinct & clearly remembered onset, with pain becoming continuous and unremitting within 24 hrs
    * present/persistent for at least three months
    * another diagnosis does not better explain
* rule out secondary causes
* may have migrainous or tension features

### secondary headache disorders

* medication-induced/associated
* pseudotumor
* cervicogenic
* low CSF
* chronic meningitis
* mass lesion

## Plan

### further workup (if indicated)

* neuroimaging
* labwork

### treatment

* acute treatments
    * NSAIDS - use no more than 2x/week
    * triptan
* prophylactic treatments
    * cyproheptadine
    * topiramate
* modifying / lifestyle factors
    * avoid caffeine overuse
    * maintain regular sleep schedule
    * fluid intake
    * eat regular meals without skipping meals

rtnv (time frame?)?  

references:

* The International Classification of Headache Disorders 2013 - doi: 10.1177/0333102413485658
* UpToDate: Pathophysiology, clinical manifestations, and diagnosis of migraine in adults. accessed 2/2017
