# FLOSMED: Free / Libre Open Source Medicine
Most medical protocols, templates, and guidelines are referenced in online databases such as UpToDate, textbooks, and peer-reviewed journals. These are rarely free or cheap, academic institutions paying significant cost for access to these. This cost exists despite taxpayer money funding a significant, if not bulk, of the studies from which these guidelines are derived. Access restrictions to such data that is funded by taxpayers has been challenged by individuals such as [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz), but little progress been made in making this data more readily available. Full versions of medical reference libraries including UpToDate are available on torrent sites such as [Library Genesis](https://en.wikipedia.org/wiki/Library_Genesis) or [The Pirate Bay](https://en.wikipedia.org/wiki/The_Pirate_Bay), in forms that can be loaded onto mobile phones. However, use of such downloads constitutes copyright violation. Outside of expensive paywalls or illegal downloads there exists little formalized organization of free and open source versions of medical protocols, templates, and guidelines. Fosmed.com aims to contribute to this vacuum. Note many in-progress articles are not online yet, as it must be ensured that enough sources are synthesized to avoid plagiarism/copyright-violation.

#### Our sites
* main git page: https://gitlab.com/deepthought/fosmed/
* mirror to git: http://www.fosmed.com
* front-end development: http://flosmed.com
    * eventually this will be main site to make it easier for those not familiar with git to use content

#### contributing
If you are interested in contributing you can do so via:
* using github if you know how
* contacting us via fosmed_website - at - circuitblue.com

#### related free and open source medical resources:

* [Cureus](http://www.cureus.com/) - free and open source medical journal with CC-BY license
* EMRs:
    * [OpenMRS](https://openmrs.org/) [[git]](https://github.com/openmrs) - MPL-2 license (GPL-compatible)
    * [FreeMED](http://freemedsoftware.org/) [[git]](https://github.com/freemed) - GPL license
    * [OpenEMR](https://www.open-emr.org/) [[git]](https://github.com/openemr) - GPL license
    * [GNU Health](http://health.gnu.org/) [git?] - website confusing, can't find git page
* [OpenBCI](http://openbci.com/) - free and open source software and hardware for a rudimentary/experimental EEG setup

#### the FREE in open source

* this refers to free as in free speech, not as in free beer
* "open access" journals that preclude redistribution, modifying/remixing, are NOT free
* see [gnu.org](https://www.gnu.org) for more information
