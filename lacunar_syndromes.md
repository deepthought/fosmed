# cerebral small vessel disease / lacunar strokes

## lacunar stroke definition/epidemiology

* definition: small subcortical infarcts < 20mm diamter (some studies use 15, others 20) in deep pentrating artery territory
* over time such small subcortical infarcts -> lacunes
* associated w *cerebral small vessel disease*: refers to syndrome affecting arterioles, capillaries, and venules
* associated with intrinsic small artery disease with significant risk factors being diabetes and hypertension
* constitute *~25% of ischemic strokes*
* if identified in chronic phase - lacunes may be residual lesion from small hemorrhage, vs. ischemic lacune
    * leading to recommendation of renaming such lacunes as "lacunes of presumed vascular origin" (which may cause more confusion since hemorrhages are technically of "vascular origin")
* radiographic mimics include large perivascular spaces (when many larger perivascular spaces clinicaly significance is unclear/controversial)
* may be clinically silent, incidentally noted radiographically

## lacunar stroke syndromes

Common syndromes include pure motor, pure sensory, sensorimotor, ataxia hemiparesis, and dysarthria clumsy hand syndrome - with localizations including carona radiata, internal capsule, basal ganglia, thalamus, pons as follows.

### pure-motor / dysarthria hemiparesis

* most common ~45-57% of lacunar syndromes
* signs/symptoms: contralateral hemiparesis (typically affecting face/arm/leg), may have dysarthria
* localizations / vasculature:
    * posterior limb of internal capsule - lenticulosstriate arteries, ant choroidal, PCA perforators
    * basis pontis (ventral or basilar part of pons where corticospinal tract runs) - basilar artery ventral penetrating branches
    * corona radiata - MCA small branches
    * cerebral peduncle: anterior/ventral midbrain - small proximial PCA branches

### sensorimotor aka thalamocapsular

* ~15-20%
* signs/symptom: symptoms of thalamic & pure motor hemiparesis strokes - contralateral weakness & numbness
* localization: posterior limb internal capsule + thalamus (either VPL or thalamic somatosensory radiation - i.e. thalamocortical fibers)
* vasculature: same as pure sensory

### pure sensory aka thalamic

* 7-18% of lacunar syndromes
* signs/symptoms: contralateral hemisensory loss to all modalities
* localization: ventroposterolateral (VPL) nucleus of thalamus
* vasculature:
    * thalamoperforator branches of PCA
    * lencticulostriate branches from MCA

### ataxic hemiparesis

* 3-18% of lacunar syndromes
* same as pure-motor/dysarthria hemiparesis, but with ataxia on same side as weakness
* localizations:
    * posterior limb of internal capsule - MCA lenticulostriate branches
    * basilar pons - basilar artery perforators

### dysarthria clumsy hand

* less common: 2-6 % of lacunar syndromes
* signs/symptoms: slurred speech & contralateral hand weakness (typically fine motor)
* localization: basilar pons between rostral 1/3 & caudal 2/3 - basilar perforators

### hemiballismus/hemichorea aka basal ganglia

* contralateral limb flailing or dyskinesia
* localization:
    * subthalamic nucleus - perforating lenticulostriate arteries
    * others: caudate, putamen, globus pallidus - anterior choroidal, Heubner's

## etiologies

* main risk factors = HTN & DM
* main mechanism thought "intrinsic" small vessel disease
    * lipohyalinosis or fibrinoid necrosis
    * associated with diffuse disruption of blood brain barrier and, with arterioles, narrowing and thickening due to deposition of plasma components
    * thought most common cause of lacunar stroke
* other mechanisms:
    * embolus from large artery or cardiac sources - less common in lacunar vs. other stroke types
    * atheroma in perforating or parent artery


## workup

* "not possible to identify cause of recent lacunar stroke based on size, shape, or location"

## distinguishing from other stroke types

* generally differentiate from cortical strokes via:
    * lack of (generally) cortical signs:
        * aphasia, neglect, homonymous visual field deficits
        * cortical sensory signs including: agraphesthesia, astereognosis, loss of 2-point discrimination
    * presence of (generally) subcortical signs: hemi-body face/arm/leg, brainstem/cerebellar signs
    * note above presence/absence of these factors is not absolute

## treatment

* no clear benefit from dual antiplatelet: http://stroke.ahajournals.org/content/44/3/861.long
* changes in circadian blood pressure with lacunar stroke: http://journals.sagepub.com/doi/abs/10.1111/j.1747-4949.2009.00314.x?url_ver=Z39.88-2003&rfr_id=ori%3Arid%3Acrossref.org&rfr_dat=cr_pub%3Dpubmed&

## specific trial data

* SPS3 trial: Predictors of stroke recurrence in patients with recent lacunar stroke (<20mm) and response to interventions according to risk status: Secondary Prevention of Small Subcortical Strokes
    * impact of aggressive BP control (SBP <130 v 130-149) and dual antiplatelet therapy (ASA+clopidogrel v ASA+placebo) on recurrent lacunar stroke risk in 3020 patients
    * no clear impact with tighter BP control or dual vs. single antiplatelet therapy

## sources

* CVD continuum 2017 "Clinical Evaluation of Patient with Acute Stroke"
* Blumenfeld
* UpToDate including: "Lacunar Infarcts"
* SPS3 trial https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3858405/
* http://svn.bmj.com/content/early/2016/09/30/svn-2016-000035
* tertiary/lay references (to be eventually replaced with more primary references):
    * http://emedicine.medscape.com/article/322992-overview
    * wiki lacunar stroke syndromes: https://en.wikipedia.org/wiki/Lacunar_stroke
