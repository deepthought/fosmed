# stroke workup / management

for non-emergent management (i.e. post brain attack)

## highlights/synopsis for inpatient management post brain attack

* common etiology differential: cardioembolic, large artery occlusive disease, small vessel
* monitoring:
    * neuro & vitals checks per institutional protocol
    * pulsox and telemetry monitoring (particularly for identification for thrombogenic arrhythmias)
* labs:
    * A1c, lipid panel, thyroid panel (e.g. sensitive TSH & free T4)
    * if patient is not on a statin then obtain LFTs and CK for baseline prior to statin initiation
    * ESR/CRP if any suspicion for temporal arteritis (based on age and clinical presentation)
* diagnostics:
    * consider MRI depending on clinical situation
    * vascular evaluation - MRA, CTA, or carotid ultrasound
    * echocardiography
* treatment:
    * BP management: GENERALLY target normotension (<140/90) with caveats:
        * acutely in 1st 24 hrs if no tPA: permissive hypertension up to 220
        * perfusion-dependent conditions requiring more gradual BP reduction:
    * consultations: stroke RN, PT, OT, ST, case-management
    * glucose control: < 180 & A1c < 7
    * statin based on initial LDL & stroke mechanism (not simply targeting < 70 anymore)
    * antithrombotic:
        * anticoagulant if:
            * mechanism though a-fib or venous-arterial (e.g. large R to L heart shunting with DVT identified)
            * taking into account CHADSVASC vs. HASBLED
            * initiation time based on comorbidities & size of stroke re bleed risk
        * antiplatelet
            * naive and no anticoagulation: ASA 81
            * intracranial stenosis: dual antiplatelet for 90 days then single antiplatelet
* followup: neuro return visit, smoking cessation, dietary changes, exercise, consider sleep apnea evaluation, 30-day heart rhythm monitor (if indicated)

## history / risk factors
### pertinent risk factors

* HTN
* HLD
* DM
* prior stroke/TIA
* OSA
* heart disease (particularly atrial fibrillation)
* cancer or blood disorders (that may be associated with coagulopathy)
* hypothyroidism
* smoking
* family history
* gender: men > women;
* "race": "black" ("African American") ~2.5x & "Hispanic" ~2x vs. "white" ("Caucasian") [operationalizing groups as such, despite being common, is arguably scientifically ungrounded for reasons beyond the scope of this work]

### pertinent mimic factors

* seizure
* migraine
* hypoglycemia

## stroke mechanism / etiology differential

### most common stroke mechanisms:

Three most prevalent categories:
* cardioembolic/a-fib (~20-30% of ischemic strokes)
* large artery atherosclerosis / thrombotic disease (~15% of ischemic strokes)
    * two main mechanisms: thrombotic stenosis & athero-emboli (artery-artery)
    * location categories:
        * extracranial:
            * asymptomatic & symptomatic carotid stenosis
            * vertebral disease
        * intracranial
* small vessel disease (~1/3)
    * note usually involves also above mechanisms
    * parenchymal or leptomeningeal vessels diameter ~5um to 2mm
    * lipohyalinosis (lipid hyaline buildup secondary to HTN) & fibrinoid degeneration
    * atheromatous disease (at origin or parent artery)

* cryptogenic - mechanism not clearly identified (historically ~30%)

(percentages primarily based on Continuum 2017)

### stroke in the "young" (<50 yo) - (mainly from Stroke in Young Continuum 2017):

#### most common

* coaguloapathy (17% cardioembolic disease in young with 1st time ischemic stroke)
* dissection (carotid, vertebral, intracranial; ~13% cervical artery dissection in young with 1st time ischemic stroke)
* PFO (in ~20% general population, ~50% in young stroke patients)

#### other considerations:
* re coagulopathy, these include:
    * MTHFR mutations including C677T which increases homocysteine
    * sustained elevated factor VIII activity associated w arterial & venous thrombosis
* fibromuscular dysplasia
    * medial fibroplasia (most common dysplastic type)
    * "string of beads" appearance w beading larger than nl caliber artery at middle-distal artery. involves media w generally preserved intima/internal-elastic-lamina/adventitia.
    * predisposition to dissection where intimal tear allows hematoma formation in media
    * locations: cervical C1-2 common. intracranial rare. bilat lesions in ~2/3.
    * may have associated saccular aneurysms
    * systemic involvement: renal
    * other types:
        * perimedial fibroplasia: focal stenosis & multiple constrictions w robust collaterals.
        * intimal fibroplasia: less than 10% of arterial fibrodysplasia pts
        * adventitial (periarterial) hyperplasia: rarest type

### Specific etiologies re presenting features:

* multifocal strokes covering multiple vascular distributions:
    * cardioembolic
    * vasculitis
* lacunar stroke:
    * small vessel factors: hypertension & diabetes
    * note large parent vessel pathology often culprit (e.g. origin athero or embolic occlusion in parent vessel)
* watershed distribution - hypoperfusion
    * global (bilateral watershed)
    * unilateral / anterior - consider ICA stenosis
* trauma, neck pain, or horner's - consider dissection
* prominent headache in older patient - consider giant cell arteritis

### small vessel disease & capsular warning syndrome

#### capsular warning syndrome

* recurrent transient lacunar syndromes that usually proceed to capsular infarction
    * >40% infarct - higher vs. other TIA types
* inter-episode window may represent intervenable window
* etiology estimate breakdown (rounded from Camps-Renom 2014):
    * ~75% small vessel disease
    * ~10% large artery atherosclerosis
    * ~10% cardioembolic
    * ~5% unclear etiology

## diagnostics

### serologies / basic labs:
* for pretty much everyone:
    * fasting lipids
    * thyroid screen (e.g. S-TSH and free T4)
    * A1c (diabetes control or occult diabetes)
* stroke in young (<50):
    * ESR/CRP
    * urine drug screen
    * thrombophilia screen
        * inherited:
            * factor V Leiden
            * activated protein C resistance
            * prothrombin mutation
            * protein C deficiency (functional assay)
            * protein S deficiency (functional assay & free/total levels)
            * antithrombin III deficiency
        * acquired: antiphospholipid syndrome
            * lupus anticoagulant (LA) (included in some hospital thrombophilia assays)
            * anti-cardiolipin (aCL) IgG & IgM (not always in hospital thrombophilia assays)
            * anti-beta2-glycoprotein 1 IgG & IgM (not always in hospital thrombophilia assays)
    * lipoprotein a - might be useful
    * homocysteine
* stroke in children
    * liprotein A strongly associated with arterial ischemic stroke in children
        * association in adults less clear
* if considering GCA/PMR or vasculitis: ESR/CRP

### radiography

* echocardiography:
    * transthoracic (TTE) or transesophageal (TEE)
    * important findings: evaluate for clot, hypokinesis / low EF), PFO/shunt, atrial enlargement, valvular abnormalities
    * consider TEE in young
* if PFO or any right to left shunting identified:
    * lower extremity ultrasound to evaluate for DVT - that may have been origin of venous to arterial embolus
    * if recent upper extremity lines or specific risk factors for upper ext DVT - consider upper extremity US as well
* evaluate vasculature for evidence of stenosis:
    * can consider carotid ultrasound if anterior stroke
        * ASYMPTOMATIC carotid stenosis (e.g. contralateral side) associated with ~2-3% annual risk of ipsilateral stroke - reduced to ~1% with intervention (endarterectomy, angioplasty, or stenting - with estimated procedural complication risk of ~1.5-3%) (as per Mayo Board Review Book)
    * MRA head and neck (e.g. if getting MRI anyways)
        * note contrast/gadolinium typically employed for visualization of proximal vasculature (neck with contrast) and TOF intracranial (head without)
    * CTA head and neck

## treatment / monitoring

* monitoring:
    * vitals
    * neuro checks
    * glucose
    * telemetry
* BP management:
    * within 1st 24 hrs from event:
        * didn't receive tPA: permissive hypertension up to SBP 220/120
        * did receive tPA: BP under 180/105
    * after 1st 24 hrs
        * non-lacunar: goal normotension < 140/90
        * lacunar: goal < 130/90 (see Continuum Stroke Epidemiology article from 2/2017)
        * caveats re not dramatically/rapidly lowering BP
            * normal perfusion auto-regulatory mechanisms may be dysfunctional such that a more graded reduction may be required in some patients - consider this if in context of BP reduction there is a significant worsening in symptoms (i.e. with improvement with Trendelenburg & IV hydration). still target eventual normotension! however may have to target achieving this at a slower rate than the normal post-24 hour permissive hypertension time period. there are no clear guidelines for this.
            * fallacies:
                * "this is normal [higher] range for this patient" - will lead to increased stroke incidence (with rare exception)
                * "once out of the hospital away from white coat syndrome will be better" - labile blood pressure associated with even worse outcomes, thus controlling in white coat scenario of even more benefit
            * see hypertensive emergency guidelines
        * specific agents: no conclusive data. general recommendation: diuretics, ACEIs. amlodipine associated w reduced BP variability & atenolol with more - variability associated with worse outcomes (Continuum as above)
* glucose management: target < 180 (stricter control under study re potential benefit)
* antithrombotic agent (see section below for details)
    * anticoagulation if indicated
    * antiplatelet agent otherwise (or in addition depending on scenario): ASA, dipyridamole, or clopidogrel
        * dual antiplatelet for 90 days if evidence of intracranial stenosis, then monotherapy
* statin - consider starting if not on one (see below re statins)
* atrial fibrillation
    * if detected consider anticoagulation, taking into account CHADS2 or CHADS2VASC vs. HASDBLED score and any other relevant patient factors re benefit/risk ratio
    * note: evidence of some benefits with apixabax over other agents
* PFO closure - controversial when to consider
    * increased risk associated with:
        * atrial septal aneurysm (> 10mm excursion during cardiac cycle)
        * thrombophilia (inherited or acquired) re parodoxical embolism (venous to arterial emboli)
    * for cryptogenic stroke in young PFO closure not routinely recommended (risks include periprocedural) - risk/benefit likely to change with different devices/procedures

## outpatient/less-acute - evaluation & management

* note - cumulative relative risk reduction of ~80% after 1st stroke/TIA may be achieved with risk factor modifications including ASA, statin, anti-hypertensives, exercise, and diet changes (2/2017 continuum)

### diagnostics

* consider 30-day outpatient heart monitor to evaluate for thrombogenic arrhythmia
    * different brands/types - e.g. MCOT
* consider sleep apnea evaluation
    * associated w increased stroke risk, higher post-stroke mortality, and worse functional outcome
    * (note: no clear benefit to acute CPAP treatment)

### on specific drug therapies

* antiplatelet therpaies
    * ASA
        * associated w ~15% relative risk reduction (95% CI 6-23%)
        * no clear evidence for significant difference btwn 81 & 325 mg
    * dipyridamole/ASA combo
        * dipyridamole 200mg & ASA 25mg  vs. ASA 25mg bid (ESPS-2 trial) - combo w ~23% risk reduction but more side effects (HA, GI)
        * AHA/ASA guidelines state ok for ASA monotherapy or this combo
    * clopidogrel (plavix) 75mg
    * considerations for regimen failure
        * underlying condition/ischemic-mechanism warranting anticoagulation
        * interactions:
            * concurrent use of agents decreasing efficacy: NSAIDs or COX-2 inhibitors
            * re clopidogrel: impaired metabolism due to genetics (e.g. CYP2C19 variant) or other meds (PPI)
        * non-adherence, poor absorption, or "poor response (resistance)"
    * antiplatelet agent if evidence of intracranial stenosis
        * dual antiplalelet therapy for 90 days (e.g. ASA 81 + clopidogrel 75) then monotherapy thereafter
* cholesterol lower agents - controversy re LDL targeting or not
    * general recommendations have been to target LDL < 70
    * new focus to use high or medium potency statins that lower cholest by >=%50 or ~30-50%, respectively
    * most likely to benefit from high/medium potency are those w:
        * clinical atherosclerotic cardiovascular disease (atherosclerotic stroke/TIA or h/o coronary artery disease)
        * LDL > 190 mg/dL
        * DM age 40-75yo and LDL > 70
        * estimated 10yr atherosclerotic dz risk >=7.5% in non-diabetics aged 40-75 w/o clinical atherosclerotic CVD
    * "high intensity" statin tx for LDL >100 if stroke/TIA presumed atherosclerotic
    * "moderate-intensity" statin recommended for patients older than 75 or if high risk of statin-related complications
    * re statin myopath: in ~7-29%. more likely in women, older/frailer, surgery, multi meds
    * relevant trials: SPARCL trial showed atorvastatin 80 vs. placebo decreased risk of ischemic stroke & myocardial infarction
    * PCSK9 inhibitors
        * PCSK9 binds to LDL receptor, with inhibition of this associated with lower LDL
        * monoclonal antibodies to PCSK9 under study as statin alternative for reducing CVD risk:
            * evolocumab (Amgen)
            * bocoizumab (Pfizer):
                * study in reducing major cardiovascular events terminated early due to lack of evidence of benefit ([SPIRE-2](https://clinicaltrials.gov/ct2/show/NCT01975389) & [SPIRE-1](https://clinicaltrials.gov/ct2/show/NCT01975376))
            * alirocumab (Aventis/Regeneron):
                * under study for acute coronary syndrome ([ODYSSEY trial](https://clinicaltrials.gov/ct2/show/NCT01663402))
* anticoagulation if indicated
    * indications include:
        * thrombogenic arrhythmia such as a-fib
        * mechanical heart valve
        * left ventricular or atrial thrombus
        * cerebral venous thrombosis
    * specific agents
        * new oral anticoagulants with reported improved efficacy & safety over warfarin
            * note w trials many exclude pts w recent stroke (e.g. in 2 wks prior)
    * timing on initiation related to risk of intracranial hemorrhage:
        * minimum of 24 hrs post-tPA
        * increased size of infarct - generally longer time waited prior to anticoagulation initiation
    * bridging:
        * may use ASA (most commonly; w dc once therapeutic), heparin, or lovenox
    * size infarct correlates w risk of hemorrhagic transformation - for vit K agents (expecting delay in full effect)
        * consider 3-5 days small infarcts (less than ~1/3 MCA), 7-10 moderate (up to ~1/3 MCA), 14 days large (complete/near-complete MCA)
        * not evidenced based
* no e/o benefit to ASA on top of coumadin
* glucose control
    * target A1c < 7% (American Diabetes Association genreal recommendations)
    * initial PO regimen: metformin 500 bid (don't start in hospital if doing more testing re contrast/renal-protection)
* if OSA: CPAP if indicated; avoidance of sedatives, hypnotics, EtOH; avoiding sleeping on back
* rehab / physical therapy / occupational therapy if indicated

### followup & lifestyle modifications

* dietary changes:
    * reduced saturated fat
    * reduced salt intake (via BP reduction - w reduction to < 2400 mg/dL & further improvement if < 1500 & < 1000 mg/dL)
    * DASH diet
    * mediterranean diet
* reduced any excessive EtOH consumption
* smoking cessation
* exercise / physical activity: recommended 3-4 ~40min sessions of moderate-vigorous aerobic exercise per week
* obesity associated w worse outcomes (increase over BMI 20)
    * though no specific evidence-based stroke reduction w weight loss (more that associated w more concrete stroke risk factors)
* sleep apnea evaluation should be considered (particularly if history of snoring or obesity)

* go to ED immediately for symptoms suspicious for stroke

### cholesterol lowering agents (mainly from 2/2017 continuum)

#### high potency statins (estimated >= 50% reduction in LDL)

* atorvastatin 40-80
* rosuvastatin 20-40

#### moderate potency statins (estimated 30-50% reduction in LDL)

* atorvastatin 10-20
* rosuvastatin 5-10
* simvastatin 20-40
* pravastatin 40-80
* lovastatin 40
* fluvastatin XL 80, or regular 40 bid
* pitavastatin 2-4

## complications

* depression - common. SSRIs may treat both depression & motor recovery (if applicable re symptoms/signs). associated w L-side lesions
* mania (~<1% of strokes)
    * associated w R-hemispheric lesions affecting ventral limbic circuit: orbitofrontal & basotemporal cortices, dorsomedial thalamus, caudate head
* seizures in ~2-4%

## references include
* 2015 cerebrovascular continuum including:
    * "Evaluation and Management of Acute Ischemic Stroke." Khatri, Pooja MD, MSc. Continuum. DOI: doi: 10.1212/01.CON.0000446101.44302.47.
    * "Inherited and Uncommon Causes of Stroke"
* 2/2017 CV continuum
* uptodate including:
    * Initial assessment and management of acute stroke
    * Etiology, classification, and epidemiology of stroke
* re capsular warning:
    * Camps-Renom 2014. PMID 25523826
    * Donnan, 1993. PMID 8492952
* Mayo Board Review Book
