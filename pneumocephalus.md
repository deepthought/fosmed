# pneumocephalus

* keywords: neurology, neurosurgery

## definition

* air in the intracranial vault

## etiologies

* trauma - head/face
* surgery/procedures
    * lumbar puncture
    * ear/nose/throat
* non-invasive positive pressure ventilation in context of recent sinus trauma/surgery
* spontaneous
* rare: [bacterial meningitis](https://www.ncbi.nlm.nih.gov/pubmed/19738168)

## mechanisms of air trapping

* inverted bottle
    * pathway formed for CSF to flow from intracranial space to extracranial space (CSF fistula)
    * when ICP exceeds extracranial pressure (ECP), CSF flows out
    * when ICP later decreased (e.g. from head position being raised or reduced brain edema), pressure gradient reverses & this can lead to air flowing intracranially
    * incoming air gravity partitions with CSF in brain
    * when gradient re-reverses, CSF will outflow rather than air (air remains intracranially trapped)
* ball-valve
    * skull defect sealed by dural flap
    * when ICP decreases air flows in
    * when ICP increases flap closes precluding air or CSF escape

## classification

* tension/symptomatic vs. asymptomatic
* "tension" if symptoms attributable to pneumocephalus
    * associated w increased ICP and/or midline shift

## symptoms/signs

* nausea/vomiting
* seizures
* dizziness
* encephalopathy/somnolence
* sensorimotor deficits

## neuroradiography

* CT hypodensity / MRI hypointensity
* depending on circumstances, may take form of "Mount Fuji" sign: frontal lobe compression w tented appearance

![pneumocephalus 1](flosmed_images/pneumocephalus1.jpg)

Bifrontal pneumocephalus. Source: Radswiki.net via Radiopaedia rID 11788 at https://radiopaedia.org/cases/11788

![pneumocephalus 2](flosmed_images/pneumocephalus2.jpg)

Bifrontal & posterior pneumocephalus. Source: Dr. Maxime St-Amant via Radiopaedia case 18492 at https://radiopaedia.org/cases/18492

![pneumocephalus 3](flosmed_images/pneumocephalus3.jpg)

Severe multifocal pneumocephalus. Source: Dr. Chris O'Donnell via Radiopaedia case 30922 at https://radiopaedia.org/cases/30922

## treatment

* head of bed 30 degrees - decrease ICP
* limit Valsalva, sneezing, cough, strain - limit ICP spikes

## oxygen
* hi oxygen administration
* mechanism of O2 decreasing pneumocephalus
    * reduces nitrogen blood concentration -> increased resorption of pneumocephalus (which is mostly nitrogen)
* modalities:
    * high flow nasal cannula, humidified
        * bad if post paranasal sinus surgery (e.g. trans-sphenoidal)
    * non-rebreather
    * hyperbaric oxygen
    * endotracheal tube
* if severe:
    * burr hole, needle aspiration, surgical closure of dural defect (if applicable)

## refs
* Oxygen Therapy with High-Flow Nasal Cannula as an Effective Treatment for Perioperative Pneumocephalus: Case Illustrations and Pathophysiological Review.
Siegel et al. Neurocrit Care. 2017 Sep 20. doi: 10.1007/s12028-017-0464-x.
* Teaching neuroimages: pneumocephalus due to bacterial meningitis. Hama-Amin AD et al. Neurology. 2009 Sep 8;73(10):e53. doi: 10.1212/WNL.0b013e3181b6bbcf.
