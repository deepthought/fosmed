# status epilepticus

## diagnosis of convulsive status epilepticus

either of following:

* &ge; 5 minutes of continuous seizure activity
* &ge; 2 discrete seizures with incomplete recovery of consciousness in between

## diagnosis of non-convulsive status epilepticus (NCSE) (from Beniczky et al.)

* epileptic discharges (EDs) qualified as following:
    * spikes, poly spikes, sharp-waves, sharp-and-slow-wave complexes

#### without known epileptic encephalopathy

one of the following:

* EDs > 2.5 Hz
* EDs < 2.5 OR rhythmic delta/theta activity (>0.5 Hz) AND one of the following:
    * improvement in EEG & clinical status after IV anti-seizure drug (ASD)
        * consider as possible NCSE if: EEG improvement w/o clinical improvement OR fluctuation w/o definite evolution
    * subtle clinical ictal phenomena during aforementioned EEG patterns
    * typical spatiotemporal evolution, e.g.:
        * incrementing onset (increase in voltage & change in frequency)
        * evolution in pattern (change in frequency > 1 Hz or change in location)
        * decrementing termination (voltage or frequency)

#### with known encephalopathy

* increase in prominence or frequency of above features vs. baseline with observable change in clinical state
* improvement of clinical & EEG features with IV ASD

## treatment

### stabilization

* assess airway, breathing circulation
* oxygen
* cardiac monitoring w telemetry
* fingerstick glucose
    * if fingerstick < 60 mg/dl then:
        * adults 100mg thiamine IV then 50mL D50W IV
        * children &ge; 2 years: 2 ml/kg D25W IV & children < 2 years: 4 ml/kg 12.5W
* basic labs (electrolytes, CBC, tox screen, if relevant ASD levels)

### ASD step 1 = 1st line fast-onset/short-acting = benzos

* can use ativan IV, diazepam IV, or midazolam IM

* lorazepam = ativan IV
    * adult: 0.1mg/kg
    * peds: 0.05-0.5mg/kg
    * rate: 2mg/min
    * max recommended for adult & peds = 4mg. may repeat in ~5-10min (some sources state only repeat once, eg. Glauser et al.)

* midazolam IM
    * 13-40kg: 5mg
    * >40kg: 10mg
    * (some sources state use single dose, e.g. Glauser et al.)

* diazepam IV
    * 0.15-0.2mg/kg/dose
    * max 10mg/dose
    * can repeat once

* if not of above available:
    * IV phenobarbital 15 mg/kg/dose
    * rectal diazepam 0.2-0.5mg/kg, max 20mg/dose
    * midazolam: intranasal, buccal

### ASD step 2 = 1st line longer-acting anti-seizure drug (semantics/convention: sometimes called 2nd line)

* fosphenytoin or phenytoin IV
    * adults & peds: 20mg/kg (some state 18-20mg/kg)
    * max 1500mgPE/dose
    * rate: fosphenytoin 150mg/min or phenytoin 50mg/min
    * effective ~10-30min after admin

* valproate as alternative
    * load: 20-25mg/kg IV
    * max 3000mg/dose
    * rate: adults: 3-6mg/kg/min & peds 1.5-3mg/kg/min

* levetiracetam (keppra)
    * load ~1.5g (~1-3g per uptodate) at 2-5 mg/kg/mins
        * others, e.g. Glauser et al., cite 60mg/kg with max 4500mg/dose

* if above not available:
    * IV phenobarbital 15mg/kg

*note: levetiracetam (keppra) & lacosamide not recommended as 1st/2nd line agents for status
    * however keppra is nonetheless widely used given relatively favorable side effect profile

### ASD step 3 if warranted - repeat another dose of the same longer-acting ASD

* may bolus ~1/3 prior dose (i.e. phenytoin or valproate)
    * can give ~5-10mg/kg

* note: while not in formal algorithms, in practice another agent might be considered (i.e. if initially gave keppra, might then try phenytoin if hemodynamically stable)

### ASD step 4 if warranted - sedatives/anesthetics

* consider intubation / ICU and infusion of anesthetic agent:
    * midazolam
    * propofol
    * pentobarbital
    * thiopental

* treat to burst suppression for 12-48 hrs
* then gradually withdraw w continuous EEG monitoring

## references include

* 2013 Beniczky et al. article in Epilepsia ("Unified EEG terminology and criteria for nonconvulsive status epilepticus")
* uptodate.com including "Convulsive status epilepticus in adults: Classification, clinical features, and diagnosis"
* Mayo Clinic Neurology Board Review book ISBN-13 978-0190244897
* Neurology Continuum
* Evidence-Based Guideline: Treatment of Convulsive Status Epilepticus in Children and Adults: Report of the Guideline Committee of the American Epilepsy Society. Glauser et al. 2017 http://dx.doi.org/10.5698/1535-7597-16.1.48 http://www.epilepsycurrents.org/doi/full/10.5698/1535-7597-16.1.48?code=amep-site
