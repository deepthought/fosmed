# spinal muscular atrophy (SMA)
* neurodegenerative disorder characterized by:
    * degeneration of spinal cord anterior horn cells & lower brainstem motor nuclei
    * diffuse symmetric progressive muscle weakness, atrophy, and hypotonia worse in lower ext
    * deletions or mutations in SMN1 gene on chromosome 5q13

## epidemiology
* ~1/11k live births vs. 4-10/100k (utd)
* carrier ~1:40 (lecture) v 1/90 to 1/47 (utd)
* prev ~35k (lecture)
* most common monogenic cause of infant mortality

## genetics / pathophysiology
* autosomal recessive monogenic disease
* most = biallelic deletions or mutations in survival motor neuron SMN1 gene on chromosome 5q13
    * most commonly deletion of exon 7 (homozygous deletions in ~94% of clinically typical SMA)
* genetic defects result in deficiency of SMN1 protein
* SMN1 protein seems involved in motor neuron mRNA synthesis & may also inhibit apoptosis
* rare non-5q SMA with heterogeneous genetics & clinical manifestations
* developmental role: need of SMN seems more important earlier in development, perhaps supported by greater efficacy of treatments when performed earlier in disease course

### SMN2 "modifying gene"
* in close proximity to SMN1 (SMN2 is centromeric to SMN1, SMN1 sometimes referred to as "telomeric SMN") - "neighbor inverse repeat"
* nearly identical to SMN1: varies by 5 nucleotides
    * salient difference in exon 7 of C-to-T 1-nucleotide change
* most mRNA from SMN2 results in truncated/non-functional SMN protein, but ~10% results in functional protein
    * due to the variation in exon 7 - leads to skipping of exon 7
    * 90% of the mRNA is unstable and gets degraded
* full length small % protein produced from SMN2 is identical to that from SMN1 (just less)
* the few % of functional SMN2-derived SMN protein allows partial compensation for the SMN1 mutations
* "rescue" or "backup" gene: without SMN2, SMN1 defects would be lethal
* variable copy numbers of SMN2 result in variable capacity to compensate for SMN1 defects (~0 to 8 copies in normal population)
* SMN2 copy number strongly predicts SMA survival
    * 2 copies of SMN2 = high likelihood type 1 phenotype
    * 3 or more copies of SMN2 associated w milder phenotype w SMN1 defects
    * 100% survival: 4-5 copies
    * vs. 1 copy in early onset - poor px w few wks survival
        * still a small percentage survives

### other modifying genes
* PLS3
* COR1C

## non-5q spinal muscular atrophies
* rare, ~< 5% SMA w genetic/clinical heterogeneity
* classification
    * distal: distal SMA / distal hereditary motor neuropathies w autosomal recessive & dominant inheritance
        * slowly progressive length-dependent weakness presenting ~ < 20 yo
        * recurrent laryngeal nerve may be affected but otherwise bulbar-sparing
        * electrophysiology: distal chronic motor axon loss w sparing of sensory (vs. e.g. Charcot-Marie-Tooth)
        * generally absent/reduced reflexes
    * proximal: proximal SMA
        * lower limb affected more than upper
        * infancy to adulthood presentation
    * bulbar: spinal & bulbar muscular atrophies / SMA-plus types w AR & X-linked inheritance
* genetic overlap of distal & proximal w some forms of ALS (DCTN1, SETX for distal; SETX, VAPB for proximal), muscular dystrophy (proximal w LMNA), and Sandhoff disease (proximal w HEXB)

### pathology
* thread-like motor roots vs. normal sensory roots
* micro histo: less motor neurons
* while disease most apparent in anterior spinal cord, pathology affects thalamus, motor cortex, and dentate

## phenotypes
* diffuse symmetric progressive muscle weakness & atrophy worse in lower ext
* decreased DTRs
* restrictive respiratory insufficiency
* less common: sleep disturbances
* normal cognition - pure motor disorder

## phenotypes - subtypes
* subtypes based on age of onset & severity of course

### type 0: prenatal
* generally have only one copy of SMN2
* pregnancy: decrease or loss of fetal movement
* onset at birth: severe weak/hypotonic, often areflexic w facial diplegia
* may have arthrogyposis (joint contractures)
* fail to achieve any motor milestones
* infant presentation may have e/o fetal hypokinesia deformation sequence: polyhydramnios, intrauterine growth retardation, skeletal abnormalities, pulmonary hypoplasia
* death by resp failure w/i 6 months, usually by 1 month
* may have congenital heart defects
* < 5% of SMA cases
* (historically classified as type 1)

### type 1 = infantile aka Werdnig-Hoffmann disease
* ~2-3 copies of SMN2
* onset: birth to 6 months
* spectrum of onset/survival w earlier onset = higher morbidity and decreased survival
* generally rapid symptom progression / severe course
* upper cranial nerves usually spared but other bulbar weakness (cry, suck, swallow, tongue)
* unable to sit without support or crawl/walk
* developmental arrest, loss of motor function
* trouble supporting head being pulled up or tilted
* frog-leg posture
* paradoxical breathing
* MSK deformities
* death common from resp failure by 2 yo
    * without significant support in ~30% by 2 yrs
* ~45% of SMA (lecture ~60%)

### type 2 = intermediate form aka Dubowitz disease
* ~3 copies SMN2
* onset: 3-18 months
    * continuum before 18 months
    * utd 3-15 months
    * lecture 6-18 months
* never independent standing/walking
* may be able to sit but with loss of sitting in teenage years
* trouble bringing arms over head
* scoliosis from muscle weakness
* variable life expectancy w ~2/3 alive at age 25
* ~20% of SMA (utd + continuum, vs. lecture ~27%)

### type 3 aka juvenile form or Kugelberg-Welander disease
* ~3-4 copies SMN2
* onset: after 18 months old, could be in adulthood
* variable onset/severity/course: as w other types earlier is worse
* proximal leg > arm weakness w falls or stair trouble (e.g. using hands, greater difficulty going down)
* Gower's maneuver to stand up
* slow walking/running, trouble jumping in place
* most lose walking/standing
* normal lifespan
* ~30% SMA
* e/o neurogenic group atrophy

### type 4 = adult onset
* ~4-8 copies SMN2
* mildest form
* ambulation usually maintained
* lifespan normal
* < ~5% SMA


## history
* 1st characterized in 1890s
* 1995 genes discovered
* anti-sense oligos ~2005


## diagnosis
* targeted mutation testing for homozygous exon 7 deletions in SMN1
* if only 1 deletion (non-homozygous) then may consider sequencing

### supplementary / older
* EMG/biopsies less useful now in context of genetic testing
* EMG/NCS shows:
    * spontaneous fibrillations & positive sharp waves
    * increased mean duration & amplitude of MUPs w polyphasia
    * abnormal neuromuscular junction transmission: decremental NCS response
* muscle bx: atrophic type 1 & 2 fibers among fascicles of hypertrophied type 1 fibers (latter reinnervated) - may not see this early on

may not get full rescue phenotype if neglect tx of prox CNS

abnl NM jn transmission in SMA
abnl boutons

### newborn screening
* ~8 cents/sample estimate
* may be added to status-quo newborn screening panel
* might work via: SMA1 gene & copy number of SMA2

## treatment
### supportive
* significantly increases lifespan - ~60s% vs. ~20-30s% for some subtypes
* respiratory support similar to as with ALS
* kyphoscloiosis - "growing rods" to avoid thoracic compression/"collapse"

### splicing modifiers
#### nusinersen
* antisense oligonucleotide administered intrathecally to motor neurons
* increases functional SMN from SMN2 gene via promoting inclusion of exon 7 region
    * via displacing factors that otherwise inhibit exon 7 splice site inclusion
* dosing: loading dosing aimed at "saturating" motor neurons then subsequent lower maintenance doses (possibly life-long)
    * generally ~3-4 loading doses then ~q4 month maintenance dosing
* FDA approval 12/2016 after trials starting ~2011
* trials
    * phase III: type 1 (ENDEAR) & type 2 (CHERISH) symptomatic
    * phase II: pre-symptomatic phase I (NURTURE) - all alive > 1 yr w/o needing vent/trach, hitting milestones. w extra SMN2 copies - still doing better w treatment
    * initial phase II open label: increased permanent vent-free survival, improving on motor function tests rather than natural decline
* electrophysiologic biomarker: tx increases CMAPs vs. natural history of decline
* early treatment associated with increased survival and improved motor function - presymptomatic tx dramatically better course vs later tx
* divergence of sham v tx groups only after ~6 months
* benefits type 1 young and older type 2

#### RG7916 & branaplam
* in trials = PO drugs
* may get to muscle being PO, unlike nusinersen targeting neurons exclusively
* RG7916: increases smn mRNA & ~2.5x increase in protein level in serum
* branaplame/o toxicity, but studies ongoing

### SMN2 upregulators
* phenylbutyrate, hydroxyurea, valproate - in vitro, but minimal clinical efficacy

### SMN1 gene therapy/replacement
* avxs101, gene replacement, in development
* scAAV9 virus vector - neuronal promotor & smn1 transgene
* phase 1: n=15. improved survival & motor fn. earlier tx still much better
* similar AAV vector in monkeys/pigs - e/o toxicity in liver & axons

### non-SMN pathway - downstream fx/modifications
#### olexoxime (trophos/roche) - mitochondrial target - phase 3
non-SMN
* mitochondrial stabilizer for motor neurons, possibly muscle
* PO
* phase II: type 2 & non-amb type 3 ages 3-25. seeming dz stabilization

#### CK2127107 - cytokientics. troponin upregulator - works at muscle level
* troponin - improves muscle fatigue?
* PO
* phase II: types 2 & 3. age 12+. results not in yet

## prognosis / monitoring
### CHOP-INTEND
* scores severity of SMA type 1
* natural history is progressive decline

## refs include
* Continuum 10/2016: ALS and Other Motor Neuron Disorders
* Richard Finkel lecture 2018
* UpToDate: Spinal muscular atrophy
* https://www.togetherinsma-hcp.com/en_us/home/disease-education/spinal-muscular-atrophy.html has good accessible illustrations
* Severe Toxicity in Nonhuman Primates and Piglets Following High-Dose Intravenous Administration of an Adeno-Associated Virus Vector Expressing Human SMN. Hinderer et al. https://www.ncbi.nlm.nih.gov/pubmed/29378426
