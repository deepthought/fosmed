# atrial fibrillation

## CHA2DS2VASc

### score
* CHF (+1)
* HTN (+1)
* age: 65-74 (+1), >=75 (+2)
* DM (+1)
* stroke / TIA / thromboembolism (+2)
* vascular dz (+1)

### use
* 0 score: low risk - may not require anticoagulation
* 1: low-moderate - consider antiplatelet or anticoagulation
* >=2: "moderate-high": anticoagulation
* balance risk score against bleeding-risk scores

## CHADS2
* CHF, HTN, age >= 75, DM, prior stroke/TIA symptoms
* score vs. estimated annual stroke risk (Gage et al. 2001):
    * score 0: ~2% (1.9%)
    * score 1: ~3% (2.8%)
    * score 2: 4%
    * score 3: ~6% (5.9%)
    * score 4: ~8 (8.5%)
    * score 5: ~12% (12.5%)
    * score 6: ~18% (18.2%)
* score 0: low ischemic stroke risk. for moderate (1-2) or high(>=3) consider anticoagulation

## refs
* Validation of clinical classification schemes for predicting stroke: results from the National Registry of Atrial Fibrillation. Gage BF et al. JAMA. 2001 Jun 13;285(22):2864-70. https://www.ncbi.nlm.nih.gov/pubmed/11401607
